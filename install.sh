#!/bin/sh
set -e

sudo install astley /usr/local/bin/astley
sudo install rick /usr/local/bin/rick
sudo install rickc /usr/local/bin/rickc
sudo install rickmake /usr/local/bin/rickmake
