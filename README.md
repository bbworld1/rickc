# rickc

Just runs a compiler/command,  but with a chance of rickrolling you instead.

## Installation:

Clone and run `./install.sh.`

## Usage:

plain rustc: `rick rustc prog.rs`
cargo: `rick cargo`

The general `rick` command allows you to add Rick Astley to any command you want:

`rick cmake ..`

`alias make="rick make"`

You can configure how often it will rickroll you by setting the $RICKNESS environment variable.
$RICKNESS is an integer between 0-32767; 0 = never rickroll, 32767 = rickroll 100% of the time.

e.g.
`export RICKNESS=16384 # rickrolls half the time`

The default $RICKNESS (if your env var is unset) is 3277 (1/10 of the time).

## Credits

Credits to https://github.com/keroserene/rickrollrc for the original rickroll terminal script.
